import java.time.LocalDate;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {

    static HashMap<String, Stock> portfolio = new HashMap<>();

    public static void main(String[] args) {
        startTest();
    }

    //This method is only for testing purposes
    private static void startTest(){
        Scanner sc = new Scanner(System.in);

        System.out.println("Type the start date with format YYYY-MM-DD: ");
        String inputStartDate = sc.nextLine();
        System.out.println("Type the end date with format YYYY-MM-DD: ");
        String inputEndDate = sc.nextLine();

        LocalDate startDate = LocalDate.parse(inputStartDate);
        LocalDate endDate = LocalDate.parse(inputEndDate);

        buildPortfolio(startDate, endDate);

        System.out.println("Type the date with format YYYY-MM-DD to retrieve the price: ");
        String inputTargetDate = sc.nextLine();
        LocalDate targetDate = LocalDate.parse(inputTargetDate);
        int stockPrice = getStockPriceForDate(targetDate);
        System.out.println("The stock price on " + targetDate + " is " + stockPrice);

        try {
            System.out.println("Initial investment amount: " + getStockPriceForDate(startDate));
            System.out.println("Ending investment amount: " + getStockPriceForDate(endDate));
            System.out.println("Number of days: " + DAYS.between(startDate, endDate));
            System.out.println("The annualized return percentage was: " + getProfit(startDate, endDate));
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    //This method is only for testing purposes
    public static int getRandomStockPrice() {
        int minPrice = 500;
        int maxPrice = 1000;
        Random random = new Random();
        return random.nextInt(maxPrice - minPrice) + minPrice;
    }

    private static void buildPortfolio(LocalDate startDate, LocalDate endDate) {
        long numberOfDays = DAYS.between(startDate, endDate);
        LocalDate currentDate = startDate;
        for (int i=0; i<=numberOfDays; i++) {
            Stock stock = new Stock("CompanyName", getRandomStockPrice());
            portfolio.put(currentDate.toString(), stock);
            currentDate = currentDate.plusDays(1);
        }
    }

    private static int getStockPriceForDate(LocalDate targetDate) {
        return portfolio.get(targetDate.toString()).getPrice();
    }

    private static double getProfit(LocalDate startDate, LocalDate endDate) throws IllegalArgumentException {
        if (!validProfitDates(startDate, endDate)) {
            throw new IllegalArgumentException("Cannot calculate annualized returns, period less than 1 year");
        }
        double annualizedReturns = 0;
        double startStockPrice = getStockPriceForDate(startDate);
        double endStockPrice = getStockPriceForDate(endDate);
        double numberOfDays = DAYS.between(startDate, endDate);
        double returns = endStockPrice - startStockPrice;
        double periods = 365 / numberOfDays;
        annualizedReturns = (Math.pow(((startStockPrice + returns) / startStockPrice), (1/periods)) - 1) * 100;
        return annualizedReturns;
    }

    //As per The Global Investment Performance Standards portfolios or composites
    //for periods of less than one year may not be annualized.
    private static boolean validProfitDates(LocalDate startDate, LocalDate endDate) {
        return DAYS.between(startDate, endDate) >= 365;
    }
}

